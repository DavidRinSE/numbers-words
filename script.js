let outliers = {
    10: "ten",
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen"
}
let ones = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
let tens = ["", "teen", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"]

function numbersToWords(number) {
    if(isNaN(parseInt(number))) {
        return "Input is not a number"
    }
    let output = ""
    let digits = number.toString().length

    switch (digits) {
        case 1: 
            return ones[number].charAt(0).toUpperCase() + ones[number].slice(1)
            break;
        case 2: 
            return twoDigits(number, true)
            break;
        case 3:
            return threeDigits(number, true)
            break;
        case 4: 
            return fourDigits(number, true)
            break;
        default: 
            break;
    }
}

function twoDigits(number, capitalize) {
    if(number < 20){
        let output;
        if(capitalize){
            output = outliers[number].charAt(0).toUpperCase() + outliers[number].slice(1)
        }else {
            output = outliers[number]
        }   
        return output
    }else {
        let output;
        let ending = parseInt(number.toString().charAt(1))
        if(capitalize){
            output = tens[parseInt(number.toString().charAt(0))].charAt(0).toUpperCase() + tens[parseInt(number.toString().charAt(0))].slice(1)
        }else {
            output = tens[parseInt(number.toString().charAt(0))]
        }
        if(ending){
            output += " " + ones[ending]
        }
        return output
    }
}

function threeDigits(number, capitalize){
    let output = ""
    let hundred = ones[parseInt(number.toString().charAt(0))]
    let ten = parseInt(number.toString().charAt(1))
    let one = parseInt(number.toString().charAt(2))
    let ending = parseInt(ten.toString() + one.toString())
    if(capitalize){
        output += hundred.charAt(0).toUpperCase() + hundred.slice(1) + " hundred"
    } else {
        output += hundred + " hundred"
    }
    if (ten > 0){
        output += " and " + twoDigits(ending, false)
    }else if (one > 0) {
        output += " and " + ones[one]
    }
    return output
}

function fourDigits(number){
    let output = ""
    let thousand = ones[parseInt(number.toString().charAt(0))]
    let hundred = parseInt(number.toString().charAt(1))
    let ending = parseInt(number.toString().slice(1))

    output += thousand.charAt(0).toUpperCase() + thousand.slice(1) + " thousand"
    if(ending > 0){
        output += " " + threeDigits(ending, false)
    }
    return output
}
let firstSet = []
let secondSet = []
let thirdSet = []
let fourthSet = []

for(let i = 1; i <= 20; i++){
    firstSet.push(numbersToWords(i))
}
for(let i = 21; i <= 100; i++){
    secondSet.push(numbersToWords(i))
}
for(let i = 101; i <= 1000; i++){
    thirdSet.push(numbersToWords(i))
}
for(let i = 1001; i <= 9999; i++){
    fourthSet.push(numbersToWords(i))
}

let firstSetElem = document.createElement('div')
firstSetElem.className = "answer"
let secondSetElem = document.createElement('div')
secondSetElem.className = "answer"
let thirdSetElem = document.createElement('div')
thirdSetElem.className = "answer"
let fourthSetElem = document.createElement('div')
fourthSetElem.className = "answer"

let firstTitle = document.createElement("h1")
firstTitle.appendChild(document.createTextNode("1 through 20"))
firstSetElem.appendChild(firstTitle)
let secondTitle = document.createElement("h1")
secondTitle.appendChild(document.createTextNode("21 through 100"))
secondSetElem.appendChild(secondTitle)
let thirdTitle = document.createElement("h1")
thirdTitle.appendChild(document.createTextNode("101 through 1000"))
thirdSetElem.appendChild(thirdTitle)
let fourthTitle = document.createElement("h1")
fourthTitle.appendChild(document.createTextNode("1001 through 9999"))
fourthSetElem.appendChild(fourthTitle)

firstSetElem.appendChild(document.createTextNode(firstSet.join(", ")))
secondSetElem.appendChild(document.createTextNode(secondSet.join(", ")))
thirdSetElem.appendChild(document.createTextNode(thirdSet.join(", ")))
fourthSetElem.appendChild(document.createTextNode(fourthSet.join(", ")))

let destination = document.getElementById('root')
destination.appendChild(firstSetElem)
destination.appendChild(secondSetElem)
destination.appendChild(thirdSetElem)
destination.appendChild(fourthSetElem)

